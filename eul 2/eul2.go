package main

/**
* Problem 1
* If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
* The sum of these multiples is 23.
* Find the sum of all the multiples of 3 or 5 below 1000.
**/

import "fmt"

func solve() int {
	limit := 1000
	sum := 0
	for i := 0; i < limit; i++ {
		if (i % 3) == 0 {
			fmt.Printf("%d is divisible by 3\n", i)
			sum += i
		} else if (i % 5) == 0 {
			fmt.Printf("%d is divisible by 5\n", i)
			sum += i
		}
	}
	return sum
}

func main() {
	fmt.Println("Project Euler Problem 1")
	total := solve()
	fmt.Printf("Total is %d\n", total)
}
